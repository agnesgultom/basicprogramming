<!DOCTYPE html>
<html>
<head>
	<title>Kalkulator</title>
</head>
<body>
<h3>Kalkulator</h3>
<form method="POST" action="<?php echo $_SERVER["PHP_SELF"];?>">
	Inputan  : <input type="text" name="string"> 

	<button type="submit" name="submit" value="submit">Kirim</button>

</form>
<?php
	function kalkulator($string){
		$operator = array();
		for($i=0; $i<=strlen($string)-1;$i++){
			if(($string[$i]=="+") || ($string[$i]=="-") || ($string[$i]=="*") || ($string[$i]=="/")){
				$operator[]=$string[$i];
			}
		}

		$string = str_replace("+", " ", $string);
		$string = str_replace("-", " ", $string);
		$string = str_replace("*", " ", $string);
		$string = str_replace("/", " ", $string);

		$operand = explode(" ", $string);

		$hasil = $operand[0];

		for($i=0; $i<=count($operator)-1; $i++){
			if($operator[$i] == "+") $hasil = $hasil + $operand[$i+1];
			else if ($operator[$i] == "-") $hasil = $hasil - $operand[$i+1];
		    else if ($operator[$i] == "*") $hasil = $hasil * $operand[$i+1];
		    else if ($operator[$i] == "/") {
		    	try {
		    		if($operand[$i+1] == 0){
		    			throw new Exception("Pembagian nol");
		    		}
		    		$hasil = $hasil / $operand[$i+1];
		    	} catch (Exception $e) {
		    		$hasil = "Tidak dapat dibagi";
		    	}
		    }
		}

		echo("Hasil = $hasil");
	
	}
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$string1 = $_POST["string"];
		$string = str_replace(' ', '', $string1);
		kalkulator($string);
	}

?>

</body>
</html>